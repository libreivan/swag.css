[![no github badge](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page/) ![MIT license](https://img.shields.io/badge/License-MIT-blue)

# [swag.css](https://codeberg.org/libreivan/swag.css/) <img src="src/swag.png" alt="drawing" width="100" align="right"/>
This is the most swag CSS stylesheet. It is written from scratch, with the help of [readable.css](https://readable-css.freedomtowrite.org/). It is an ever-improving and changing stylesheet, designed to be swag.

### Know your rights
This stylesheet is under the MIT license:

- **Freedom to Use**: You have the right to use the software for any purpose, whether it's personal, academic, or commercial.
- **Freedom to Modify**: You can modify the source code of the software to suit your needs or preferences.
- **Freedom to Distribute**: You have the right to distribute the software, whether in its original form or modified, to others.
- **Collaboration**: You can collaborate with us on the software's development and improvement.
- **No License Compatibility Issues**: You can combine the MIT-licensed software with other software, even if they use different licenses.
- **No Usage Restrictions**: There are no restrictions on the technologies or fields of use, giving you maximum flexibility.
- **No Royalties**: You are not required to pay any royalties or fees for using, modifying, or distributing the software.

### Contributing guidelines
There are several ways to contribute to this project:

- Reporting issues
- Discussing potential improvements
- Contributing code
- Writing documentation
- Submitting feature requests
- Providing feedback

When making any sort of contribution, please make sure to abide by [Forgejo's Code of Conduct](https://codeberg.org/forgejo/code-of-conduct). If you don't have the time to read it, just know that all you have to do is be nice, and you'll be fine.

*</> with <3 by [libreivan](https://libreivan.com/) and [daudix](https://daudix.codeberg.page/).*
