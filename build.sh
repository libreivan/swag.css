#!/usr/bin/env bash

INPUT="src"
OUTPUT="_output"
DEMO="demo"

# Clean up the corpses
rm -f -r $OUTPUT/*
rm -f -r $DEMO/minified/*

sass $INPUT:$OUTPUT/default
sass $INPUT:$OUTPUT/minified --style=compressed

cp -r "$OUTPUT/minified" "$DEMO"

# This "build system" is deceptively simple (for now).
